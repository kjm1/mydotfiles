-- Data

import Data.Monoid
import Data.Tree
import qualified Data.Map as M
import System.Exit (exitSuccess)
import System.IO (hPutStrLn)
import XMonad
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (nextScreen, prevScreen)
import XMonad.Actions.MouseResize
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.UpdatePointer
import XMonad.Actions.GridSelect

-- Hooks

import XMonad.Hooks.DynamicLog (PP (..), dynamicLogWithPP, shorten, wrap, xmobarColor, xmobarPP)
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks (ToggleStruts (..), avoidStruts, docksEventHook, manageDocks)
import XMonad.Hooks.ManageHelpers (doFullFloat, isFullscreen)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.InsertPosition

-- Layouts

import XMonad.Layout.GridVariants (Grid (Grid))
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows)
import XMonad.Layout.MultiToggle ((??), EOT (EOT), mkToggle, single)
import qualified XMonad.Layout.MultiToggle as MT (Toggle (..))
import XMonad.Layout.MultiToggle.Instances (StdTransformers (MIRROR, NBFULL, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed (Rename (Replace), renamed)
import XMonad.Layout.ResizableTile
import XMonad.Layout.ShowWName
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spacing
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Tabbed
import qualified XMonad.Layout.ToggleLayouts as T (ToggleLayout (Toggle), toggleLayouts)
import XMonad.Layout.WindowArranger (WindowArrangerMsg (..), windowArrange)
import qualified XMonad.StackSet as W

-- Utilities

import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.Run (runProcessWithInput, spawnPipe, safeSpawn)
import XMonad.Util.SpawnOnce
import XMonad.Util.NamedScratchpad

-- Defined varibles

myModMask = mod4Mask :: KeyMask
altMask = mod1Mask :: KeyMask
myTerminal = "st" :: String
-- myTerminal = "xfce4-terminal" :: String
myEditor = myTerminal ++ " -e nvim" :: String
myBorderWidth = 2 :: Dimension
myNormColor = "#292d3e" :: String
myFocusColor = "#c792ea" :: String
myFont ="xft:Iosevka Nerd Font Mono:size=9:antialias=true:hinting=true" :: String
myfocusFollowsMouse = False :: Bool

-- Windowcount

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-- Startup programs

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "transmission-daemon &"
    spawnOnce "~/.fehbg &"  -- set last saved feh wallpaper
    -- spawnOnce "nitrogen --restore &"
    spawnOnce "compton -b &"
    spawnOnce "nm-applet &"
    spawnOnce "volumeicon &"
    spawnOnce "trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 1 --transparent true --alpha 0 --tint 0x292d3e --height 22 &"
    -- spawnOnce "urxvtd -q -o -f &"
    setWMName "LG3D"

-- Spacings

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Single window with no gaps

mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Layouts definition

tall = renamed [Replace "tall"]
    $ limitWindows 12
    $ mySpacing 4
    $ ResizableTall 1 (3 / 100) (1 / 2) []

monocle = renamed [Replace "monocle"] $ limitWindows 20 Full

grid = renamed [Replace "grid"]
    $ limitWindows 12
    $ mySpacing 4
    $ mkToggle (single MIRROR)
    $ Grid (16 / 10)

threeCol = renamed [Replace "threeCol"]
    $ limitWindows 7
    $ mySpacing' 4
    $ ThreeCol 1 (3 / 100) (1 / 3)

tabs = renamed [Replace "tabs"]
    -- I cannot add spacing to this layout because it will
    -- add spacing between window and tabs which looks bad.
    $ tabbed shrinkText myTabTheme


-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

floats = renamed [Replace "floats"] $ limitWindows 20 simplestFloat

-- Layout hook

myLayoutHook = avoidStruts
    $ smartBorders
    $ mouseResize
    $ windowArrange
    $ T.toggleLayouts floats
    $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
  where
    myDefaultLayout = 
        noBorders monocle
        ||| tall
        ||| threeCol
        ||| grid
        ||| tabs

xmobarEscape :: String -> String
xmobarEscape = concatMap doubleLts
  where
    doubleLts '<' = "<<"
    doubleLts x = [x]

myWorkspaces :: [String]
myWorkspaces = clickable . (map xmobarEscape)
    $ ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
  where
    clickable l = ["<action=xdotool key super+" ++ show (i) ++ "> " ++ ws ++ "</action>" | (i, ws) <- zip [1 .. 9] l]

-- GRID SELECT

myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
                  (0x31,0x2e,0x39) -- lowest inactive bg
                  (0x31,0x2e,0x39) -- highest inactive bg
                  (0x61,0x57,0x72) -- active bg
                  (0xc0,0xa7,0x9a) -- inactive fg
                  (0xff,0xff,0xff) -- active fg

-- gridSelect menu layout

mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
    { gs_cellheight   = 40
    , gs_cellwidth    = 200
    , gs_cellpadding  = 8
    , gs_originFractX = 0.5
    , gs_originFractY = 0.5
    , gs_font         = myFont
    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = defaultGSConfig
myScratchPads = [ NS "terminal" spawnTerm  findTerm manageTerm
                , NS "music" spawnMus findMus  manageMus
                ]
    where
    spawnTerm = myTerminal ++ " -n scratchpad"
    findTerm = resource =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h -- and I'd like it fixed using the geometry below
        where
    -- reusing these variables is ok since they're confined to their own
        -- where clauses 
        h = 0.9         -- height, 10%
        w = 0.9         -- width, 100%
        t = 0.95 - h    -- bottom edge
        l = 0.95 -w     -- centered left/right
    spawnMus = "spotify"
    findMus = className =? "Spotify"
    manageMus = customFloating $ W.RationalRect l t w h -- and I'd like it fixed using the geometry below
  	where
  	-- reusing these variables is ok since they're confined to their own
        -- where clauses
        h = 1      -- height, 10%
        w = 1      -- width, 100%
        t = 1 -h   -- bottom edge
        l = 1 -w   -- centered left/right

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out the full
     -- name of my workspaces, and the names would very long if using clickable workspaces.
     [ className =? "about"         --> doFloat
     , className =? "confirm"       --> doFloat
     , className =? "file_progress" --> doFloat
     , className =? "dialog"        --> doFloat
     , className =? "download"      --> doFloat
     , className =? "error"         --> doFloat
     , className =? "notification"  --> doFloat
     , className =? "Gimp"          --> doFloat
     , className =? "Gimp"          --> doShift ( myWorkspaces !! 6 )
     , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
     ] <+> namedScratchpadManageHook myScratchPads

myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
    where fadeAmount = 1.0

-- Keybindings

myKeys :: [(String, X ())]
myKeys =
    [
    ------------------ Window configs ------------------
    
    -- Move focus to the master window
    ("M-m", windows W.focusMaster),
    -- Move focus to the next window
    ("M-j", windows W.focusDown),
    -- Move focus to the previous window
    ("M-k", windows W.focusUp),
    -- Move focus to the next window
    ("M-S-s", windows W.swapMaster),
    -- Swap focused window with next window
    ("M-S-j", windows W.swapDown),
    -- Swap focused window with prev window
    ("M-S-k", windows W.swapUp),
    -- Kill window
    ("M-q", kill1),
    -- Kill all window
    ("M-S-q", killAll),
    -- Restart xmonad
    ("M-C-r", spawn "xmonad --restart"),
    -- Quit xmonad
    ("M-C-q", io exitSuccess),

    ----------------- Floating windows -----------------
    
    -- Toggles 'floats' layout
    ("M-f", sendMessage (T.Toggle "floats")),
    -- Push floating window back to tile
    ("M-S-f", withFocused $ windows . W.sink),
    -- Push all floating windows to tile
    ("M-C-f", sinkAll),
    
    ---------------------- Layouts ----------------------
    
    -- Switch focus to next monitor
    ("M-.", nextScreen),
    -- Switch focus to prev monitor
    ("M-,", prevScreen),
    -- Switch to next layout
    ("M-<Tab>", sendMessage NextLayout),
    -- Switch to first layout
    ("M-S-<Tab>", sendMessage FirstLayout),
    -- Toggles noborder/full
    ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts),
    -- Toggles noborder
    ("M-S-n", sendMessage $ MT.Toggle NOBORDERS),
    -- Shrink horizontal window width
    ("M-S-h", sendMessage Shrink),
    -- Expand horizontal window width
    ("M-S-l", sendMessage Expand),
    -- Shrink vertical window width
    ("M-C-j", sendMessage MirrorShrink),
    -- Exoand vertical window width
    ("M-C-k", sendMessage MirrorExpand),
    
    ---------- Grid Select
    
    (("M-S-o"), spawnSelected'
          [ ("Thunar", "thunar")
          , ("Geany", "geany")
          , ("Firefox", "firefox")
          , ("Discord", "discord")
          , ("Spotify", "spotify")
          , ("LibreOffice Writer", "lowriter")
          , ("Brave", "brave-browser")
          , ("Audio", "pavucontrol" )
          , ("Bluetooth", "blueman-manager")
          , ("Wi-Fi", "nm-connection-editor")
          , ("ST", "st")
          , ("URXVT", "urxvt")
          , ("Ranger", "urxvt -e 'ranger'")
          , ("Sxiv Image", "sxiv -q -t ~/Pictures/wallpapers/dt/")
          ]),
         ("M-S-g", goToSelected $ mygridConfig myColorizer),
         ("M-S-b", bringSelected $ mygridConfig myColorizer),
    
    -------------------- App configs --------------------
    
    -- Menu
    ("M-r", spawn "dmenu_run"),
    -- Window nav
    ("M-z", spawn "xfce4-screenshooter"),
    -- Browser
    ("M-b", spawn "brave-browser"),
    -- File explorer
    ("M-e", spawn "thunar"),
    -- Terminal
    ("M-<Return>", spawn (myTerminal)),
    --Lock
    ("M-S-m", spawn "i3lock -neft --image=/home/k-j-m/Pictures/wallpapers/br/Anime/lock.png"),
    -- Scrot
    ("M-s", spawn "spotify"),
    -- Urxvt
    ("M-;", spawn "urxvt"),
    
    --------------------Scrachpads----------------------
    
    ("M-C-<Return>",namedScratchpadAction myScratchPads "terminal"),
    ("M-C-s",namedScratchpadAction myScratchPads "music"),
    
    --------------------- Hardware ---------------------
    
    -- Volume
    ("<XF86AudioLowerVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%"),
    ("<XF86AudioRaiseVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%"),
    ("<XF86AudioMute>", spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle" ),
    
    -- Brightness
    ("<XF86MonBrightnessUp>", spawn "brightnessctl set +10%"),
    ("<XF86MonBrightnessDown>", spawn "brightnessctl set 10%-")
    ]

-- Main function

main :: IO ()
main = do
    -- Xmobar
    xmobarLaptop <- spawnPipe "xmobar -x 0 ~/.config/xmobar/primary.hs"
    xmobarMonitor <- spawnPipe "xmobar -x 1 ~/.config/xmobar/secondary1.hs"
    -- Xmonad
    xmonad $ ewmh def {
        manageHook = (isFullscreen --> doFullFloat) <+> myManageHook <+> manageDocks <+> insertPosition Below Newer <+> namedScratchpadManageHook myScratchPads,
        handleEventHook = docksEventHook,
        modMask = myModMask,
        terminal = myTerminal,
        startupHook = myStartupHook,
        layoutHook = myLayoutHook,
        workspaces = myWorkspaces,
        borderWidth = myBorderWidth,
        normalBorderColor = myNormColor,
        focusedBorderColor = myFocusColor,
        focusFollowsMouse = myfocusFollowsMouse,
    -- Log hook
        logHook = workspaceHistoryHook <+> myLogHook <+> dynamicLogWithPP xmobarPP {
            ppOutput = \x -> hPutStrLn xmobarLaptop x >> hPutStrLn xmobarMonitor x,
            -- Current workspace in xmobar
            ppCurrent = xmobarColor "#c3e88d" "" . wrap "[" " ]",
            -- Visible but not current workspace
            ppVisible = xmobarColor "#c3e88d" "",
            -- Hidden workspaces in xmobar
            ppHidden = xmobarColor "#82AAFF" "",
            -- Hidden workspaces (no windows)
            ppHiddenNoWindows = xmobarColor "#c792ea" "",
            -- Title of active window in xmobar
            ppTitle = xmobarColor "#6272a4" "" . shorten 55,
            -- Separators in xmobar
            ppSep = "<fc=#666666> | </fc>",
            -- Urgent workspace
            ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!",
            -- Number of windows in current workspace
            ppExtras = [windowCount],
            ppOrder = \(ws : l : t : ex) -> [ws,l] ++ex++ [t]
        } >> updatePointer (0.5, 0.5) (0.5, 0.5) 
} `additionalKeysP` myKeys
