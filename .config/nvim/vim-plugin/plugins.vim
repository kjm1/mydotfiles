call plug#begin('~/.config/nvim/autoload/plugged')

" Better Syntax Support
    Plug 'sheerun/vim-polyglot'
    " File Explorer
    Plug 'vifm/vifm.vim'
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    " Start Page
    Plug 'mhinz/vim-startify'
    Plug 'itchyny/lightline.vim'
    Plug 'etdev/vim-hexcolor'
    Plug 'ervandew/supertab'
    Plug 'vim-syntastic/syntastic'
    Plug 'junegunn/fzf.vim', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    Plug 'Raimondi/delimitMate'
    Plug 'airblade/vim-rooter'
    Plug 'mbbill/undotree'
    Plug 'sirver/ultisnips'
    Plug 'octol/vim-cpp-enhanced-highlight'
    Plug 'vimwiki/vimwiki'
    Plug 'kien/ctrlp.vim'
    Plug 'rdnetto/ycm-generator'


call plug#end()
