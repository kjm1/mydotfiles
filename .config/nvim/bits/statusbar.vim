let g:currentmode={
       \ 'n'  : 'NORMAL ',
       \ 'v'  : 'VISUAL ',
       \ 'V'  : 'V·Line ',
       \ '^V' : 'V·Block ',
       \ 'i'  : 'INSERT ',
       \ 'R'  : 'R ',
       \ 'Rv' : 'V·Replace ',
       \ 'c'  : 'Command ',
       \}
"StatusLINE
set statusline=
set statusline+=%1*
set statusline+=\ %{toupper(g:currentmode[mode()])}
set statusline+=%2*
set statusline+=\ %F
set statusline+=%3*
set statusline+=\ %r 
set statusline+=%4*
set statusline+=\ %M
set statusline+=%=    ""RIGHTSIDE
"set statusline+=%5*
set statusline+=\ %y
set statusline+=%6*
set statusline+=\ [%p%%]
set statusline+=%7*
set statusline+=\ [%c:%l/%L]
set statusline+=%8*
set statusline+=\ [%n]

highlight Cursor ctermbg=DarkGray
