syntax on
filetype on
filetype indent on
filetype plugin indent on
set nu
set hidden
set relativenumber
set nohlsearch
set noshowmode
set incsearch
set nowrap
set guicursor=
set noerrorbells
set smartindent
set shiftwidth=4
set expandtab
set tabstop=4 softtabstop=4
set nobackup
set noswapfile
set undodir=~/.cache/nvim/undodiri
set undofile
set scrolloff=5
set signcolumn=yes
set title
set ruler
set showcmd
set wildmenu
set smartcase
set ignorecase
set fillchars+=vert:\ 
set updatetime=300
set timeoutlen=500
set splitbelow splitright
set laststatus=2
set backspace=indent,eol,start
set shortmess=atI
