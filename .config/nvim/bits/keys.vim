let mapleader="\<Space>"
nnoremap <leader>w :w<cr>
nnoremap <leader>f :EditVifm<cr>
nnoremap <leader>t :botright vertical terminal<cr>
"Extra movements
nnoremap <leader>h :wincmd h<cr>
nnoremap <leader>j :wincmd j<cr>
nnoremap <leader>k :wincmd k<cr>
nnoremap <leader>l :wincmd l<cr>

" Others
nnoremap <leader>u :UndotreeShow<cr>

"Splits
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>

noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

"Terminal inside Vim/Nvim
tnoremap <C-[> <C-\><C-N>
