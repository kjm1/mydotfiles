Config { font = "xft:Fira Code Regular Nerd Font:pixelsize=12:antialias=true:hinting=true"
                , additionalFonts = ["xft:FontAwesome:pixelsize=12:antialias=true:hinting=true"
                                    ,"xft:Roboto Mono Nerd Font:pixelsize=12:antialias=true:hinting=true"
                                    ,"xft:Iosevka Nerd Font Mono:pixelsize=12:antialias=true:hinting=true"
                                    ]
                , bgColor     = "#282A36"
                , fgColor     = "#ff6c6b"
                , borderColor  = "#151515"
                , border      = NoBorder
                , alpha       = 120/255
                , textOffset  = -1
                , iconOffset  = -1
                , position    = Top
                , lowerOnStart = True
                , hideOnStart  = False
                , allDesktops  = True
                , overrideRedirect = True
                , persistent   = True
                , iconRoot     = "/home/k-j-m/.xmonad/xpm/"
                , commands = [ Run UnsafeStdinReader
                    , Run Weather "VOTV" ["-t", "<fn=1>\xf185</fn> <tempC>C"] 36000
              --      , Run Network "tun0" ["-t", "(VPN <rx>KB)"] 20
                    , Run Network "wlp3s0" ["-t", "<fn=1>\xf0aa</fn> <rx>kb <fn=1>\xf0ab</fn> <tx>kb"] 150
                    , Run Memory ["-t", "<fn=1>\xf233</fn> <used>M (<usedratio>%)"] 150
                    , Run Cpu ["-t", "<fn=1>\xf108</fn> (<total>%)","-H","50","--high","red"] 150
                    , Run Date "<fn=1>\xf133</fn> %d %b %Y %H:%M " "date" 600
                    , Run CoreTemp ["--template" , "<fn=1>\xf2c9</fn> <core0>C"] 50
                    , Run Com "/home/k-j-m/.config/xmobar/trayer-padding-icon.sh" [] "trayerpad" 20
                    , Run Com "/bin/bash" ["-c", "~/.xmonad/getvolume" ] "myvolume" 9
                    ]
       , template = "<action='dmenu_run'><fc=#b3b3b3><icon=haskell_20.xpm/></fc></action> | %UnsafeStdinReader%}{ \
           \ <action=`wpa_gui`><fc=#ee009a>%wlp3s0%</fc></action> |   \
           \ <action=`st -e htop`><fc=#9a00ee>%cpu% %coretemp%</fc> | \
           \ <fc=#009aee>%memory%</fc></action> | \
           \ <action=`xdg-open http://forecast.io/`><fc=#00ee9a>%VOTV%</fc></action> | \
           \ <fc=#ffff00>vol:</fc><fc=#ffff00>%myvolume%</fc> | \
           \ <action=`xdg-open https://www.google.com/calendar/`><fc=#ee9a00>%date%</fc></action> | \
           \%trayerpad%"
       }
