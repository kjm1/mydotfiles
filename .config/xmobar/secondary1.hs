Config { 

   -- appearance
     font =         "xft:Hack-Bold:pixelsize=12:antialias=truehinting=true,Hack Nerd Font:pixelsize=12:antialias=true,hinting=true"
   , bgColor =      "#282A36"
   , fgColor =      "#ff6c6b"
   , alpha  =       200/255
   , position =     Top
   , border =       NoBorder
   , borderColor =  "#373b41"
       ,textOffset= -1
       ,iconOffset= -1

   -- layout
   , sepChar =  "%"   -- delineator between plugin names and straight text
   , alignSep = "}{"  -- separator between left-right alignment
   , template = "<action=~/.config/xmenu/xmenu.sh><fc=#57c7ff>  </fc></action> %UnsafeStdinReader% }{| <fc=#ee009a><action=st -e htop>%cpu%</action></fc> | <fc=#9a00ee><action=st -e htop>%memory%</action></fc> | <fc=#009aee>%internet%</fc> | <fc=#00ee9a>%backlight%</fc> | <fc=#ffff00>%volume%</fc> | <fc=#ee9a00>%clock% </fc>" 

   -- general behavior
   , lowerOnStart =     True    -- send to bottom of window stack on start
   , hideOnStart =      False   -- start with window unmapped (hidden)
   , allDesktops =      True    -- show on all desktops
   , overrideRedirect = False    -- set the Override Redirect flag (Xlib)
   , pickBroadest =     False   -- choose widest display (multi-monitor)
   , persistent =       True    -- enable/disable hiding (True = disabled), 
   , iconRoot     = "/home/k-j-m/.xmonad/xpm/"
-- plugins
   , commands = [
       --cpu 
       Run Com "/home/k-j-m/.config/xmobar/bar/cpu" [] "cpu" 150,

       --memory
       Run Com "/home/k-j-m/.config/xmobar/bar/memory" [] "memory" 150,

       --internet
       Run Com "/home/k-j-m/.config/xmobar/bar/internet" [] "internet" 25,

       --backlight
       Run Com "/home/k-j-m/.config/xmobar/bar/backlight" [] "backlight" 50,

       --volume
       Run Com "/home/k-j-m/.config/xmobar/bar/volume" [] "volume" 5,
       
       --battery
--        Run Com "/home/k-j-m/.config/xmobar/bar/battery" [] "battery" 50,

       --clock
       Run Com "/home/k-j-m/.config/xmobar/bar/clock" [] "clock" 600,
       
       --
       Run UnsafeStdinReader
                ]
       }
